---
title: "Final Report"
description: ""
lead: ""
date: 2023-08-22T20:09:13+05:00
lastmod: 2023-08-22T20:09:13+05:00
draft: false
images: []
menu:
  docs:
    parent: "r"
    identifier: "final-report-a0e729093563de47c17546329c7e65da"
weight: 1
toc: true
type: docs
---


![GCCRS Logo](https://raw.githubusercontent.com/Rust-GCC/gccrs/master/logo.png)

## Enhancing user errors & Error Code Support for GCC Rust (GCCRS) Frontend
- Organization: [GNU Compiler Collection (GCC)](https://gcc.gnu.org/)
- Mentors: [Philip Herron](https://github.com/philberty/), [Arthur Cohen](https://github.com/cohenarthur/)
---
## Description:
This project aims to improve the user experience of gnu gccrs by adding and enabling error codes that are compatible with rustc. This will enable the integration of the two test suites and allow the rustc testsuite to run on gccrs. The project will involve modifying the gccrs frontend code to produce more informative error messages and rich locations to help users and developers improve their code. 

---

## What was done:

- [x] Converted ErrorCode `struct` to an `enum class` and their respective functions like `make_description` & `make_url`, to enforce proper errors codes, and use it as an API for emitting error codes.
   - [x] Refactored error handling code to use the new error code framework.
   - [x] Also, removed the memory leak which was present in the previous error code framework.
- [x] Updated the `Error` struct to store error codes, and locations, as a wrapper function to store and emit error codes for parsing and expansion errors.
- [x] Refactored error handling code to use `rich locations`, to provide more information about the error location, and help users and developers to improve their code.
- [x] Improved Error messages to more user-friendly and informative messages.
- [x] Compared the rustc test suite on gccrs and added and fixes some of the issues along the way on some bugs and internal compiler errors along the way.

---

## Previous & Current state:

[David Malcolm](https://github.com/davidmalcolm) extended the gcc diagnostics to add support to associate diagnostics with rules from coding standards. And we are using this to associate gcc-rust error codes. [David Malcolm](https://github.com/davidmalcolm) added one single error [`E0054`](https://doc.rust-lang.org/error_codes/E0054.html) (Non-allowed `cast` to `bool`). After adding more type-checking, gccrs emits error codes & messages similiar to rustc.

```rust
fn main() {
    let x = 5;
    let x_is_nonzero = x as bool; // { dg-error "cannot cast .<integer>. as .bool." }

    0u32 as char; // { dg-error "cannot cast .u32. as .char., only .u8. can be cast as .char." }

    let x = &[1_usize, 2] as [usize]; // { dg-error "cast to unsized type: .& .usize:CAPACITY.. as ..usize.." }

    let a = &0u8; // Here, `x` is a `&u8`.
    let y: u32 = a as u32; // { dg-error "casting .& u8. as .u32. is invalid" }
}
```
### Previous State:
Previous version emits this output:
```bash
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:3:24: error: invalid cast ‘<integer>’ to ‘bool’ [E0054]
    3 |     let x_is_nonzero = x as bool; // { dg-error "cannot cast .<integer>. as .bool." }
      |                        ^    ~~~~
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:5:5: error:invalid cast ‘u32’ to ‘char’ [E0054]
    5 |     0u32 as char; // { dg-error "cannot cast .u32. as .char., only .u8. can be cast as .char." }
      |     ^~~~    ~~~~
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:7:13: error: invalid cast ‘& [usize:CAPACITY]’ to ‘[usize]’ [E0054]
    7 |     let x = &[1_usize, 2] as [usize]; // { dg-error "cast to unsized type: .& .usize:CAPACITY.. as ..usize.." }
      |             ^                ~
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:10:18: error: invalid cast ‘& u8’ to ‘u32’ [E0054]
   10 |     let y: u32 = a as u32; // { dg-error "casting .& u8. as .u32. is invalid" }
      |                  ^    ~~~
```
### Current State:
With the latest type checking code, it emits more specific error codes and error messages.

```bash
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:3:24: error: cannot cast ‘<integer>’ as ‘bool’ [E0054]
    3 |     let x_is_nonzero = x as bool; // { dg-error "cannot cast .<integer>. as .bool." }
      |                        ^    ~~~~
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:5:5: error: cannot cast ‘u32’ as ‘char’, only ‘u8’ can be cast as ‘char’ [E0604]
    5 |     0u32 as char; // { dg-error "cannot cast .u32. as .char., only .u8. can be cast as .char." }
      |     ^~~~    ~~~~
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:7:13: error: cast to unsized type: ‘& [usize:CAPACITY]’ as ‘[usize]’ [E0620]
    7 |     let x = &[1_usize, 2] as [usize]; // { dg-error "cast to unsized type: .& .usize:CAPACITY.. as ..usize.." }
      |             ^                ~
~/gccrs/gcc/testsuite/rust/compile/all-cast.rs:10:18: error: casting ‘& u8’ as ‘u32’ is invalid [E0606]
   10 |     let y: u32 = a as u32; // { dg-error "casting .& u8. as .u32. is invalid" }
      |                  ^    ~~~
```

### Bugs Finding:
One the interesting bug specifically `internal compiler error`, I found while researching errors codes on gccrs, is that it breaks when accessing access empty stack, which was a security concern and also not allowed in rustc. Now, it's fixed:
```rust
const FOO: u32 = return 0; 
```
 Now, it's fixed and emits proper error code and message, similiar to rustc.
```bash
empty-stack.rs:1:18: error: return statement outside of function body [E0572]
    1 | const FOO: u32 = return 0; // error: return statement outside of function body
      |                  ^~~~~~
```
## Pull Requests:
See the pull requests page for more details, [here](https://mahadmuhammad.github.io/gsoc/23/r/pulls/).
My opened PR's can also, be seen on [GitHub](https://github.com/Rust-GCC/gccrs/pulls?q=+is%3Apr+author%3AMahadMuhammad).

## Issues:
See the issue page for more details, [here](https://mahadmuhammad.github.io/gsoc/23/r/issues/).
My opened issues can also, be seen on [GitHub](https://github.com/Rust-GCC/gccrs/issues?q=author%3AMahadMuhammad).

## Acknowledgements
Starting and completing this project was very difficult without the help of these such amazing people. I can't thank enough to these people for their constant help and support. 
- [Philip Herron](https://github.com/philberty/) & [Arthur Cohen](https://github.com/cohenarthur/) for mentoring me throughout the project and helpful guidance through my journey.
- [Pierre Emmanuel Patry](https://github.com/P-E-P/) for code reviews and helpful suggestions. Also, [Marc Poulhiès](https://github.com/dkm), [Thomas Schwinge](https://github.com/tschwinge), [Philipp Krones](https://github.com/flip1995/) for guidance and cool tips.
- And finally, [Faisal Abbas](https://github.com/abbasfaisal/) for helping me in my first contributions till the acceptance of my proposal. 
---
title: "Refactoring"
description: "Refactoring Redefined multiple times Error function into one error function"
lead: "Refactoring Redefined multiple times Error function into one error function"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "gccrs-diagnostics"
weight: 130
toc: true
---

---

# Refactor Redefined multiple times Error function into one error function

- [ ] Creating error code class
- [ ] Refactoring `rust_error_at` function.
- [ ] Passing appropriate error code from that function
  - This is because rustc emit different error code for struct , ... etc.,


It's worth refactoring that error function, as it has almost exact same error message and pattern.

```bash
➜  gccrs git:(master) ack  'redefined multiple times'
gcc/rust/resolve/rust-ast-resolve-type.h
183:		       "generic param redefined multiple times");

gcc/rust/resolve/rust-ast-resolve-implitem.h
63:	rust_error_at (r, "redefined multiple times");
79:	rust_error_at (r, "redefined multiple times");
96:	rust_error_at (r, "redefined multiple times");
112:	rust_error_at (r, "redefined multiple times");
152:	rust_error_at (r, "redefined multiple times");
172:	rust_error_at (r, "redefined multiple times");
192:	rust_error_at (r, "redefined multiple times");
210:	rust_error_at (r, "redefined multiple times");
250:	rust_error_at (r, "redefined multiple times");
269:	rust_error_at (r, "redefined multiple times");

gcc/rust/resolve/rust-ast-resolve-toplevel.h
61:	rust_error_at (r, "redefined multiple times");
91:	rust_error_at (r, "redefined multiple times");
113:	rust_error_at (r, "redefined multiple times");
135:	rust_error_at (r, "redefined multiple times");
161:	rust_error_at (r, "redefined multiple times");
183:	rust_error_at (r, "redefined multiple times");
205:	rust_error_at (r, "redefined multiple times");
227:	rust_error_at (r, "redefined multiple times");
251:	rust_error_at (r, "redefined multiple times");
273:	rust_error_at (r, "redefined multiple times");
293:	rust_error_at (r, "redefined multiple times");
314:	rust_error_at (r, "redefined multiple times");
336:	rust_error_at (r, "redefined multiple times");
380:	rust_error_at (r, "redefined multiple times");
400:	rust_error_at (r, "redefined multiple times");
476:	rust_error_at (r, "redefined multiple times");

gcc/rust/resolve/rust-ast-resolve-expr.cc
428:	  rust_error_at (label.get_locus (), "label redefined multiple times");
502:	  rust_error_at (label.get_locus (), "label redefined multiple times");
531:	  rust_error_at (label.get_locus (), "label redefined multiple times");

gcc/rust/resolve/rust-ast-resolve-stmt.h
65:	rust_error_at (r, "redefined multiple times");
100:	rust_error_at (r, "redefined multiple times");
133:	rust_error_at (r, "redefined multiple times");
165:	rust_error_at (r, "redefined multiple times");
185:	rust_error_at (r, "redefined multiple times");
211:	rust_error_at (r, "redefined multiple times");
237:	rust_error_at (r, "redefined multiple times");
258:	rust_error_at (r, "redefined multiple times");
296:	rust_error_at (r, "redefined multiple times");
332:	rust_error_at (r, "redefined multiple times");

gcc/testsuite/rust/compile/redef_error5.rs
5:    const TEST: bool = false; // { dg-error "redefined multiple times"  }

gcc/testsuite/rust/compile/redef_error4.rs
12:    fn test() -> bool { // { dg-error "redefined multiple times" }

gcc/testsuite/rust/compile/redef_error3.rs
5:fn test() -> i32 { // { dg-error "redefined multiple times" }

gcc/testsuite/rust/compile/redef_error1.rs
6:struct S1(i32, bool); // { dg-error "redefined multiple times" }

gcc/testsuite/rust/compile/redef_error6.rs
8:    fn test(self) -> i32 { // { dg-error "redefined multiple times" }

gcc/testsuite/rust/compile/redef_error2.rs
2:const TEST: f32 = 3.0; // { dg-error "redefined multiple times" }

```
---
title: "Variadic "
description: "Variadic "
lead: "Variadic Parameters Used on Non-C ABI Function"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "gccrs-diagnostics"
weight: 130
toc: true
---

# Variadic Parameters Used on Non-C ABI Function
-  Fixing this error will improve gccrs to emit error codes similiar to rustc.
- You can view the same on [`compiler-explorer`](https://godbolt.org/#z:OYLghAFBqd5QCxAYwPYBMCmBRdBLAF1QCcAaPECAMzwBtMA7AQwFtMQByARg9KtQYEAysib0QXACx8BBAKoBnTAAUAHpwAMvAFYTStJg1DEArgoKkl9ZATwDKjdAGFUtEywYgATKUcAZPAZMADl3ACNMYhAANlIAB1QFQjsGFzcPb3jE5IEAoNCWCKjYq0wbFKECJmICNPdPH1LygUrqgjyQ8MiYyyqauozGvvbAzsLu6IBKS1QTYmR2DgBSLwBmMDAlgFYAITFaVAB3CBMGM0x0Se2AESWNAEEqBgBqFiZAiEnnpYB2Hbv7phVARIi8Vl4AEpmAjg75/AHPRHPJ7I1CoCDqZ4mAAcpGeADpCVdVjtngB6MnPSLEEgbAG/W4PBkcaa0ThbXieDhaUioThQ8zPBSzeaYb5rHikAiaFnTADW3mx%2BJ%2B0WxGi8GgAnNFNditltpGyOJJOTLeZxeAoQBopTLpnBYDBEChUCw4nRIuRKGg3R6osBkMhiAoALQKZhxBQIVAWLAANzwCwAanhMIcAPJxRicSU0Wgg4OUMJmsKBaoATxzvFLzGI5fTYW0ZWl3F4PrYgnTDFole5vCwYRMwCc%2BytrdIWDeRnEfYneGIzbwccwY55QLKJhBVfIgkwRp5tDwYWIFZcWDNBGIeBYVemVAMwAUKbTmez4/4ghEYnYUhkgkUKjqLOuhcPohjGNC%2BhHlakDTKgcS2AIY68Kgy7EFeWAwVAzBsCAFw5AwpBxmIJiLBqXg8JM0xNIhngQI4AyeKB/ijAURR6AkSS0YxHHZLRHRsd0oE0RUww8cJe6Li0wwCV0UTCWJrj1Ho5htLJ4zydRIoLBIrLsqas58hwzyqNi0QhtEkjPAA4k4TgQkIzwQLZ9mOW85iRF8EC4IQJDiqsXCTLwLZaFRpAKlsNpGiapA3pImr4nq0QUVw0SrNEGh6tqpBcjyRmWtatp9vaToQEgswEHEm5ehAPruvQxDBKwiymOYyDPFw%2BKqvia74EQGF6B%2BwiiOIv5DQBahmiBpCHCecS3vo%2Bk5WaRnppuVUEM8qBUCZZkWVZLkOU5h1uUwHnEF5Li%2Bg1/mBcFdp6cavA3pF%2BJSKsPzYti6WrJqUgaNIuUoRaliFSFsrhd4CXYqlWxcB90RSNEPySJIuJGqsBl5SD4MlfADrOnVfo1UTDUgMQUiaja8aJpgz4ZlmXK5nQBZWhAxazjWFbblzdYNk2Njbu2jAEF2PZmgOQ4jrQtBjpKk7gTOPL4Au5TLquvDrsgm6LJKgQgvuvCHsep4YIsPKXteC33kwj706%2BTO8ENX6jdI41KJNwGZAYRjk5BxtYXBCEpMhvJoRhK7wNRknNHRDFKRkzEMOg6nsaBnEEeJWRcSkqdCZYMe0a0/QJ0xBfWEXMmsXJKmKekZeqTUeeaTMcw6YFi0cByy2GZwzytQQ7WdfFPVOT5/W3UFRWhdMCCYEwWBRJ8kNcBoUWcDFL02kD5ocAVNq46QBNlSAFUbSTrr1ZETW4RwpnmZZNl2UdznP6d51T/h/V4Ogg2yC7P43ayAmkBHkugfCzSYPNVsj1u471WutTcW0doDyHviEeGgnJXSvsQfyXgp641nvPRelB5QSDXp3TeIBIo92xnvUGB8HqQ0kMPLgBpohbC8KqZGPx4ad0xrQ4G9DCGdy8FjIR91irTDQkkewkggA%3D%3D%3D)
### I tried this code from [`E0045`](https://doc.rust-lang.org/error_codes/E0045.html):

```rust
// https://doc.rust-lang.org/error_codes/E0045.html
#![allow(unused)]
fn main() {
extern "Rust" {
    fn foo(x: u8, ...); // error!
}
}
```
---
### I expected to see this happen:
- Give error like rustc:
```rust
error[E0045]: C-variadic function must have C or cdecl calling convention
 --> <source>:4:5
  |
4 |     fn foo(x: u8, ...); // error!
  |     ^^^^^^^^^^^^^^^^^^^ C-variadics require C or cdecl calling convention

error: aborting due to previous error

For more information about this error, try `rustc --explain E0045`.
Compiler returned: 1
```
---
### Instead, this happened: 
- Compiled Successfully.
```rust
Compiler returned: 0
```
--- 
### Meta

- What version of Rust GCC were you using, git sha 5406b633a1f5e6e3ae8da02589adf4ea003dec12.
- Godbolt version : `gccrs (Compiler-Explorer-Build-gcc-2f91d511200bf85558c9013b09a458c06edd1e02-binutils-2.40) 13.0.1 20230417 (experimental)`
---




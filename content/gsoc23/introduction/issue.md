---
title: "Issue"
description: "Issue page for tracking changes in gccrs-diagnostics"
lead: "The table below shows the error codes and their description"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "introduction"
weight: 130
toc: true
---

<script src="https://gist.github.com/MahadMuhammad/8c9d5fc88ea18d8c520937a8071d4185.js"></script>